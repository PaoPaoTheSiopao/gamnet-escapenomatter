﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManSkill : MonoBehaviour
{
    public PhotonView PV;

    public PlayerMovement move;

    public float skillCD;
    private float defSkillCD;
    public float skillDur;
    private float defSkillDur;

    public bool isSkill = false;
    // Start is called before the first frame update
    void Start()
    {
        defSkillCD = skillCD;
        defSkillDur = skillDur;
        move = gameObject.GetComponentInParent<PlayerMovement>();
        PV = gameObject.GetComponentInParent<PhotonView>();
    }

    // Update is called once per frame
    void Update()
    {
        if (skillCD <= 0)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Debug.Log("Space");
                if (PV.IsMine)
                {
                    UseSkill();
                    skillCD = defSkillCD;
                }
                isSkill = true;
                skillCD = defSkillCD;
            }
        }
        else
        {
            skillCD -= Time.deltaTime;
        }
        if (isSkill)
        {
            if (skillDur <= 0)
            {
                isSkill = false;
                skillDur = defSkillDur;
            }
            else
            {
                UseSkill();
                skillDur -= Time.deltaTime;
            }
        }
    }

    void UseSkill()
    {
        GameObject ghost = GameObject.FindGameObjectWithTag("Enemy");
        EnemyAI ghAI = ghost.GetComponent<EnemyAI>();
        ghAI.FollowShout(this.gameObject.transform);
        Debug.Log("Ghost : " + ghost.name);
    }
}

