﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WomanSkill : MonoBehaviour
{
    public PhotonView PV;

    public PlayerMovement move;

    public float skillCD;
    private float defSkillCD;
    public float skillDur;
    private float defSkillDur;

    public bool isSkill= false;
    // Start is called before the first frame update
    void Start()
    {
        defSkillCD = skillCD;
        defSkillDur = skillDur;
        move = gameObject.GetComponentInParent<PlayerMovement>();
        PV = gameObject.GetComponentInParent<PhotonView>();
    }

    // Update is called once per frame
    void Update()
    {
        if (skillCD <= 0)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Debug.Log("Space");
                if (PV.IsMine)
                {
                    UseSkill();
                    skillCD = defSkillCD;
                }
            }
        }
        else
        {
            skillCD -= Time.deltaTime;
        }
        if(isSkill)
        {
            if(skillDur<=0)
            {
                move.movementSpeed = 10;
                isSkill = false;
                skillDur = defSkillDur;
            }
            else
            {
                skillDur -= Time.deltaTime;
            }
        }
    }

    void UseSkill()
    {
        move.movementSpeed = 20;
        isSkill = true;
    }
}
