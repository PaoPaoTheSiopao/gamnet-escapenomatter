﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CanvasScript : MonoBehaviour
{
    public GameObject readyButton;
    public GameObject unReadyButton;
    public GameObject StartGameButton;

    public Text txt;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        txt.text = "Players In Room : " + DataSingleTon.Instance.playersInRoom;
        if (PhotonNetwork.IsMasterClient)
        {
            if (DataSingleTon.Instance.playersInRoom == DataSingleTon.Instance.playersReady)
            {
                StartGameButton.SetActive(true);
            }
        }
    }

    public void OnReadyButtonClick()
    {
        readyButton.SetActive(false);
        unReadyButton.SetActive(true);
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Player");
        foreach(GameObject i in objects)
        {
            AvatarSetup avatar = i.GetComponent<AvatarSetup>();
            avatar.ReadyAvatar();
        }
    }

    public void OnUnReadyButtonClick()
    {
        readyButton.SetActive(true);
        unReadyButton.SetActive(false);
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject i in objects)
        {
            AvatarSetup avatar = i.GetComponent<AvatarSetup>();
            avatar.ReadyAvatar();
        }
    }
}
