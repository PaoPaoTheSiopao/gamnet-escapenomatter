﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject canvas;

    public bool startedGame = false;

    public
    // Start is called before the first frame update
    void Start()
    {
        //Time.timeScale = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (DataSingleTon.Instance.playersInRoom == DataSingleTon.Instance.playerInWinArea && DataSingleTon.Instance.playersInRoom != 0) 
        {
            WinGame();
            Debug.Log("Won");
        }
    }

    public void StartGame()
    {
        if (DataSingleTon.Instance.playersInRoom == DataSingleTon.Instance.playersReady)
        {
            GameObject[] objects = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject i in objects)
            {
                AvatarSetup avatar = i.GetComponent<AvatarSetup>();
                avatar.RPCTurnOffCanvas();
            }
        }
    }

    public void WinGame()
    {
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject i in objects)
        {
            AvatarSetup avatar = i.GetComponent<AvatarSetup>();
            avatar.RPCTurnOnWinCanvas();
        }
    }

    public void PauseGame()
    {

    }
}
