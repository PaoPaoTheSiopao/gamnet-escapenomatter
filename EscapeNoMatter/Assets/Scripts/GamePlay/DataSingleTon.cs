﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataSingleTon : Singleton<DataSingleTon>
{

    public int playersInRoom = 0;
    public int playersReady = 0;

    public int playerInWinArea = 0;

    public void AddPlayerCount()
    {
        playersInRoom++;
    }

    public void DecPlayerCount()
    {
        playersInRoom--;
    }

    public void AddReadyPlayerCount()
    {
        playersReady++;
    }

    public void DecReadyPlayerCount()
    {
        playersReady--;
    }

    public void PlayerWinAreaChecker(int i)
    {
        playerInWinArea += i;
    }

    public void RemovePlayerWinArea()
    {
        playerInWinArea--;
    }

}
