﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.LWRP;

public class Thunder : MonoBehaviour
{
    private Light2D thunder;
    [Header("ThunderValues")]
    public float thunderCd;
    private float defThunderCd;
    public float sThunderCd;
    private float sDefThunderCd;
    public float lightDisSpeed;
    public float lowestLightIntensity;

    private int instance = 0;

    // Start is called before the first frame update
    void Start()
    {
        thunder = GetComponent<Light2D>();
        defThunderCd = thunderCd;
        sDefThunderCd = sThunderCd;
        thunderCd = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (thunderCd <= 0)
        {
            if (instance == 0)
            {
                thunder.intensity = 1;
                FindObjectOfType<AudioManager>().Play("Thunder");
                instance++;
            }
            if(sThunderCd<=0)
            {
                thunder.intensity = 1.5f;
                thunderCd = defThunderCd;
                sThunderCd = sDefThunderCd;
                instance = 0;
            }
            else
            {
                sThunderCd -= Time.deltaTime;
            }
        }
        else
        {
            thunderCd -= Time.deltaTime;
        }
        thunder.intensity -= lightDisSpeed * Time.deltaTime;
        if(thunder.intensity<lowestLightIntensity)
        {
            thunder.intensity = lowestLightIntensity;
        }

    }
}
