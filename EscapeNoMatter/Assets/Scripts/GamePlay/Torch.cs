﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.LWRP;

public class Torch : MonoBehaviour
{
    private Light2D torch;
    public float maxCD;
    public float maxOuter;

    // Start is called before the first frame update
    void Start()
    {
        torch = GetComponent<Light2D>();
        StartCoroutine(Flicker());
    }

    // Update is called once per frame
    void Update()
    {
        if(torch.pointLightOuterRadius<9)
        {
            torch.pointLightOuterRadius = 9f;
        }
        torch.pointLightOuterRadius -= 5 * Time.deltaTime;
    }

    IEnumerator Flicker()
    {
        torch.pointLightOuterRadius = Random.Range(9, maxOuter);
        yield return new WaitForSeconds(Random.Range(0, maxCD));
        StartCoroutine(Flicker());
    }
}
