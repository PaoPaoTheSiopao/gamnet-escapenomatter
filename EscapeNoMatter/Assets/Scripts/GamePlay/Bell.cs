﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bell : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            GameObject gh = GameObject.FindGameObjectWithTag("Enemy");
            EnemyAI ghAi = gh.GetComponent<EnemyAI>();
            ghAi.FollowShout(this.transform);
            Debug.Log("Ding Ding");
        }
    }
}
