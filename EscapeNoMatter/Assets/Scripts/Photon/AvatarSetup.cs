﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarSetup : MonoBehaviour
{

    public PhotonView PV;
    public int characterValue;
    public GameObject myCharacter;

    public int playerDamage;

    public Camera myCamera;
    public AudioListener myAL;

    // Start is called before the first frame update
    void Start()
    {
        PV = GetComponent<PhotonView>();
        if (PV.IsMine)
        {
            PV.RPC("RPC_AddCharacter", RpcTarget.AllBuffered, SelectCharacter.PI.mySelectedCharacter);
            PV.RPC("RPC_AddCharacterCount", RpcTarget.AllBuffered);
            Debug.Log("AddedPlayer");
        }
        else
        {
            Destroy(myCamera);
            Destroy(myAL);
        }
    }

    public void ReadyAvatar()
    {
        if (PV.IsMine)
        {
            PV.RPC("RPC_ReadyCharacter", RpcTarget.AllBuffered);
        }
    }

    public void UnReadyAvatar()
    {
        if (PV.IsMine)
        {
            PV.RPC("RPC_UnReadyCharacter", RpcTarget.AllBuffered);
        }
    }

    public void RPCTurnOffCanvas()
    {
        if (PV.IsMine)
        {
            PV.RPC("RPC_TurnOffCanvas", RpcTarget.AllBuffered);
        }
    }

    public void RPCTurnOnWinCanvas()
    {
        if (PV.IsMine)
        {
            PV.RPC("RPC_TurnOnWinCanvas", RpcTarget.AllBuffered);
        }
    }

    public void RPCYouDied()
    {
        if(PV.IsMine)
        {
            PV.RPC("RPC_TurnOnDiedCanvas", RpcTarget.AllBuffered);
        }
    }

    [PunRPC]
    void RPC_AddCharacter(int whichCharacter)
    {
        characterValue = whichCharacter;
        myCharacter = Instantiate(SelectCharacter.PI.allCharacters[whichCharacter], transform.position, transform.rotation, transform);
    }

    [PunRPC]
    void RPC_AddCharacterCount()
    {
        DataSingleTon.Instance.AddPlayerCount();
    }

    [PunRPC]
    void RPC_ReadyCharacter()
    {
        DataSingleTon.Instance.AddReadyPlayerCount();
    }

    [PunRPC]
    void RPC_UnReadyCharacter()
    {
        DataSingleTon.Instance.DecReadyPlayerCount();
    }

    [PunRPC]
    void RPC_TurnOffCanvas()
    {
        GameObject canvas = GameObject.FindGameObjectWithTag("Canvas");
        canvas.SetActive(false);
        Time.timeScale = 1;
    }
    [PunRPC]
    void RPC_WinAreaCheck(int i)
    {
        DataSingleTon.Instance.PlayerWinAreaChecker(i);
    }

    [PunRPC]
    void RPC_TurnOnWinCanvas()
    {
        GameObject canvas = GameObject.FindGameObjectWithTag("WinCanvas");
        canvas.SetActive(true);
    }

    [PunRPC]
    void RPC_TurnOnDiedCanvas()
    {
        DataSingleTon.Instance.DecPlayerCount();
        GameObject canvas = GameObject.FindGameObjectWithTag("DiedCanvas");
        canvas.SetActive(true);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "WinArea")
        {
            RPC_WinAreaCheck(1);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "WinArea")
        {
            RPC_WinAreaCheck(-1);
        }
    }
}
