﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class EnemyAI : MonoBehaviour
{
    [Header("GetComponents")]
    [SerializeField] public FieldOfVision fov;
    [SerializeField] private Path path;
    [SerializeField] private Seeker seeker;
    private Rigidbody2D rb;
    [Space]
    [Header("Required Fields")]
    public Transform target;
    public EnemyTerritory area;
    public Animator animator;
    public float enemSpeed;
    public float fovCD;
    [HideInInspector]public float defFovCD;
    public float chaseDuration;
    [HideInInspector] public float defChaseDuration;
    [Space]
    [Header("Seeker")]
    public float nextWaypointDistance = 3f;

    int currentWaypoint = 0;
    bool reachedEndPath = false;

    //
    
    [HideInInspector] public bool followTarget = true;
    [Header("Time to Change Roam Point")]
    public float changePntCd;
    [HideInInspector] public float defChangePntCd;
    [Header("Search Intervals")]
    public float searchInterval;
    [HideInInspector] public float defSearchInterval;

    void Start()
    {
        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody2D>();
        fov = GetComponent<FieldOfVision>();
        GameObject o = GameObject.FindGameObjectWithTag("Territory");
        area = o.GetComponent<EnemyTerritory>();

        target = area.GetRandomPointFromTerritory(this.gameObject);

        followTarget = true;
        defChangePntCd = changePntCd;
        defSearchInterval = searchInterval;
        defFovCD = fovCD;
        defChaseDuration = chaseDuration;

        InvokeRepeating("UpdatePath", 0f, .5f);
    }
    public float GetDistanceBetweenTarget()
    {
        float distance = 0;
        distance = Vector2.Distance(this.transform.position, target.transform.position);
        return distance;
    }


    private void OnEnable()
    {
        if(!seeker)
            seeker = GetComponent<Seeker>();
        if(!rb)
            rb = GetComponent<Rigidbody2D>();
    }

    void UpdatePath()
    {
        if (target != null && seeker.IsDone())
        {
            seeker.StartPath(rb.position, target.position, OnPathComplete);
        }
    }

    public void GetPointsFromTerritory()
    {
        //print("Getting Random Point");
        target = area.GetRandomPointFromTerritory(this.gameObject);
    }

    public void FollowShout(Transform origin)
    {
        print("Getting shout Point : "+origin.transform.position);
        target = area.ShoutOrigin(this.gameObject,origin);
    }

    void UpdateDirection(Vector2 dir)
    {
        if (dir.y >= 0.5f)
        {
            fov.direction = transform.up;
        }
        else if (dir.y <= -0.5f)
        {
            fov.direction = -transform.up;
        }
        else if (dir.x >= 0.5f)
        {
            fov.direction = transform.right;
        }
        else if (dir.x <= -0.5f)
        {
            fov.direction = -transform.right;
        }
    }

    void OnPathComplete(Path p)
    {
        if(!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (path == null)
            return;

        if(currentWaypoint>=path.vectorPath.Count)
        {
            reachedEndPath = true;
            return;
        }
        else
        {
            reachedEndPath = false;
        }

        Vector2 direction = ((Vector2)path.vectorPath[currentWaypoint] - rb.position).normalized;
        Vector2 force = direction * enemSpeed * Time.deltaTime;

        if (!followTarget)
        {
            direction = Vector2.zero;
        }
        rb.AddForce(force);

        float distance = Vector2.Distance(rb.position, path.vectorPath[currentWaypoint]);
        if(distance<nextWaypointDistance)
        {
            currentWaypoint++;
        }
        UpdateDirection(direction);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            AvatarSetup av = collision.GetComponent<AvatarSetup>();
            av.RPCYouDied();
        }
    }
}
