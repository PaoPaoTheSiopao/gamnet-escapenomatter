﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/PatrolDecision")]
public class Patrol_Decision : Decision
{
    public override bool Decide(StateController controller)
    {
        bool targetVisible = Look(controller);
        Debug.Log("PatrolDecide");
        return targetVisible;
    }

    // Update is called once per frame
    private bool Look(StateController controller)
    {
        if (controller.enem.fov.visiblePlayer.Count > 0)
        {
            controller.enem.target = controller.enem.fov.visiblePlayer[0];
            controller.enem.fov.visiblePlayer.Clear();
            controller.enem.fovCD = controller.enem.defFovCD;
            controller.enem.fov.enabled = false;
            return true;
        }
        return false;
    }
}
