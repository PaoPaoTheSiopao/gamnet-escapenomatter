﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTerritory : MonoBehaviour
{
    public float height;
    public float width;
    public class Point
    {
        public Transform pnt;
        public GameObject owner;
    }

    public List<Point> points = new List<Point>();

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(transform.position, new Vector3(width, height, 0));
    }
    // Start is called before the first frame update
    void Start()
    {
        if (GetComponent<Collider2D>()) {
            Collider2D box = GetComponent<Collider2D>();
            width = box.bounds.size.x;
            height = box.bounds.size.y;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Transform GetRandomPointFromTerritory(GameObject owner)
    {
        foreach (Transform child in this.transform)
        {
            if(child.gameObject.name == owner.ToString())
            {
                Destroy(child.gameObject);
            }
        }
        GameObject empt = new GameObject();
        empt.name = owner.ToString();
        empt.transform.SetParent(this.transform);
        Transform mim = empt.transform;
        mim.localPosition = new Vector3(Random.Range(this.width, -this.width)/2f,Random.Range(this.height, -this.height)/2f, 0);
        //print("point = " + mim.position);
        return mim;
    }

    public Transform ShoutOrigin(GameObject owner, Transform origin)
    {
        foreach (Transform child in this.transform)
        {
            if (child.gameObject.name == owner.ToString())
            {
                Destroy(child.gameObject);
            }
        }
        GameObject empt = new GameObject();
        empt.name = owner.ToString();
        empt.transform.SetParent(this.transform);
        Transform mim = empt.transform;
        mim.position = new Vector2(origin.transform.position.x,origin.transform.position.y);
        //print("point = " + mim.position);
        return mim;

    }
}
