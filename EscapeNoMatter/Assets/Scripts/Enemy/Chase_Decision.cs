﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/ChaseDecision")]
public class Chase_Decision : Decision
{
    public override bool Decide(StateController controller)
    {
        bool targetVisible = Look(controller);
        return targetVisible;
    }

    // Update is called once per frame
    private bool Look(StateController controller)
    {
        if(controller.enem.chaseDuration<=0)
        {

            controller.enem.chaseDuration = controller.enem.defChaseDuration;
            controller.enem.GetPointsFromTerritory();
            return true;
        }
        else
        {
            controller.enem.chaseDuration -= Time.deltaTime;
            return false;
        }
    }
}
