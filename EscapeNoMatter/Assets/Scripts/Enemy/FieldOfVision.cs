﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfVision : MonoBehaviour
{

    public float viewRadius;
    public float viewAngle;
    Collider2D[] playerInRadius;
    [SerializeField]public LayerMask obstacleMask, playerMask;
    public List<Transform> visiblePlayer = new List<Transform>();
    public Vector2 direction;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, viewRadius);
        var angle1 = Quaternion.AngleAxis(viewAngle, transform.forward) * direction * viewRadius;
        var angle2 = Quaternion.AngleAxis(-viewAngle, transform.forward) * direction * viewRadius;



        Gizmos.color = Color.blue;

        Gizmos.color = Color.magenta;
        Gizmos.DrawRay(new Vector3(transform.position.x, transform.position.y, transform.position.z), angle1);
        Gizmos.DrawRay(new Vector3(transform.position.x, transform.position.y, transform.position.z), angle2);


        Gizmos.color = Color.black;
        Gizmos.DrawRay(transform.position, direction * viewRadius);
    }
    private void Start()
    {
        direction = transform.right;
    }
    private void FixedUpdate()
    {
        FindPlayer();
    }

    public Vector2 DirFromAngle(float angleDeg, bool global)
    {
        if (!global)
        {
            angleDeg += transform.eulerAngles.z;
        }
        return new Vector2(Mathf.Sin(angleDeg * Mathf.Deg2Rad), Mathf.Cos(angleDeg * Mathf.Deg2Rad));
    }

    public void FindPlayer()
    {
        playerInRadius = Physics2D.OverlapCircleAll(transform.position, viewRadius, playerMask);
        visiblePlayer.Clear();
        //Pierre: This is causing some slowdowns;

        for(int i = 0;i< playerInRadius.Length;i++)
        {
            Transform player = playerInRadius[i].transform;
            Vector2 dirTarget = player.position - transform.position;
            //if (Vector2.Angle(dirTarget,transform.right) < viewAngle/2)
            if (Vector2.Angle(dirTarget, direction) < viewAngle/2)
            {
                float distancePlayer = Vector2.Distance(transform.position, player.position);

                if (!Physics2D.Raycast(transform.position, dirTarget, distancePlayer, obstacleMask))
                {
                    visiblePlayer.Add(player);
                    print("Detected");
                }
            }
        }
    }
}
