﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/PatrolAction")]
public class Patrol_Action : Actions
{
    public override void Act(StateController controller)
    {
        //Debug.Log("Enemy Patrol Started");
        Patrol(controller);
    }

    private void Patrol(StateController controller)
    {
        if(controller.enem.fovCD<=0)
        {
            controller.enem.fov.enabled = true;
        }
        else
        {
            controller.enem.fovCD -= Time.deltaTime;
        }
        if (controller.enem.changePntCd <= 0)
        {
            controller.enem.GetPointsFromTerritory();
            controller.enem.changePntCd = controller.enem.defChangePntCd;
        }
        else
        {
            controller.enem.changePntCd -= Time.deltaTime;
        }

    }

}
